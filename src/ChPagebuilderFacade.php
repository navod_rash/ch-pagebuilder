<?php

namespace Creativehandles\ChPagebuilder;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Creativehandles\ChPagebuilder\Skeleton\SkeletonClass
 */
class ChPagebuilderFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'ch-pagebuilder';
    }
}
