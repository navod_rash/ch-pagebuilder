<?php

namespace Creativehandles\ChPagebuilder;

use Creativehandles\ChPagebuilder\Console\BuildPageBuilderPackageCommand;
use Illuminate\Support\ServiceProvider;

class ChPagebuilderServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        /*
         * Optional methods to load your package assets
         */
         $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'ch-pagebuilder');
         $this->loadViewsFrom(__DIR__.'/../resources/views', 'ch-pagebuilder');
         $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        // $this->loadRoutesFrom(__DIR__.'/routes.php');

        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../config/config.php' => config_path('ch-pagebuilder.php'),
            ], 'config');


            // Publishing the views.
//            $this->publishes([
//                __DIR__.'/../resources/views' => resource_path('views/Admin/'),
//            ], 'views');

            //publishing routes and breadcrumbs
            $this->publishes([
                __DIR__.'/../routes' => base_path('routes/packages'),
            ], 'views');


            // Publishing the views.
            /*$this->publishes([
                __DIR__.'/../resources/views' => resource_path('views/vendor/ch-pagebuilder'),
            ], 'views');*/

            // Publishing assets.
            /*$this->publishes([
                __DIR__.'/../resources/assets' => public_path('vendor/ch-pagebuilder'),
            ], 'assets');*/

            // Publishing the translation files.
            /*$this->publishes([
                __DIR__.'/../resources/lang' => resource_path('lang/vendor/ch-pagebuilder'),
            ], 'lang');*/

            // Registering package commands.
            // Registering package commands.
            $this->commands([
                BuildPageBuilderPackageCommand::class
            ]);
        }
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        // Automatically apply the package configuration
        $this->mergeConfigFrom(__DIR__.'/../config/config.php', 'ch-pagebuilder');

        // Register the main class to use with the facade
        $this->app->singleton('ch-pagebuilder', function () {
            return new ChPagebuilder;
        });
    }
}
