<?php
namespace Creativehandles\ChPagebuilder\Repositories;

use App\Repositories\BaseEloquentRepository;
use App\User;
use Creativehandles\ChPagebuilder\Models\PageBuilder;

class PageBuilderRepository extends BaseEloquentRepository
{

    public function getModel()
    {
        return new PageBuilder();
    }

}