@extends('Admin.layout')

@section('content')

    <div class="content-header row">
        <div class="content-header-left col-md-8 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    @include('Admin.partials.breadcumbs',['header'=>__('ch-pagebuilder::pagebuilder.Pagebuilder')])
                </div>
            </div>
        </div>

        <div class="content-header-right col-md-4 col-12 mb-2">
            <div class="mb-1 pull-right">
                <a href="{{route('admin.pagebuilder.create')}}"
                   class="btn btn-secondary btn-block-sm"><i
                            class="ft-file-plus"></i> {{__('ch-pagebuilder::pagebuilder.Design new page')}}</a>
            </div>
        </div>
    </div>


    <div class="content-body">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-content show">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped full-width" id="thegrid">
                                    <thead>
                                    <tr>
                                        <th>{{__('ch-pagebuilder::pagebuilder.Id')}}</th>
                                        <th>{{__('ch-pagebuilder::pagebuilder.Identifier')}}</th>
                                        <th>{{__('ch-pagebuilder::pagebuilder.Name')}}</th>
                                        <th></th>
                                        <th></th>
                                        <th ></th>
                                        <th ></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

    <link rel="stylesheet" type="text/css" href="{{ asset("vendors/css/tables/datatable/datatables.min.css")}}">
    <script src="{{ asset("vendors/js/tables/datatable/datatables.min.js")}}"  type="text/javascript"></script>
    <script type="text/javascript">
        var theGrid = null;
        $(document).ready(function(){
            var editRoute = "{{route('admin.pagebuilder.edit',['pagebuilder'=>'sampleId'])}}";

            theGrid = $('#thegrid').DataTable({
                "bStateSave": true,
                "language": {
                    "url": "{{ asset(__('general.dataTable'))}}"
                },
                "processing": true,
                "serverSide": true,
                "paging": true,
                "pageLength": 50,
                "ordering": true,
                "order": [[ 2, "asc" ]],
                "responsive": false,
                "ajax": "{{route('admin.pageBuilderGrid')}}",
                "columnDefs": [
                    {
                        "className":"action-col","orderable": false,
                        "targets": 3
                    },
                    {
                        "className":"action-col","orderable": false,
                        "targets": 4
                    },
                    {
                        "render": function ( data, type, row ) {
                            return '<a href="'+editRoute.replace('sampleId',row[0])+'?translate=1" class="btn btn-warning btn-sm">{{__("ch-pagebuilder::pagebuilder.Translate")}}</a>';
                        },"className":"action-col","orderable": false,
                        "targets": 5
                    },
                    {
                        "render": function ( data, type, row ) {
                            var active = false;
                                    @if(\Illuminate\Support\Facades\Auth::user()->isSuperAdmin())
                            var active = true;
                                    @endif
                            var func = active ? 'onclick="return doDelete('+row[0]+')"' : '';
                            return '<a href="#"'+ func +' class="btn btn-danger btn-sm">{{__('ch-pagebuilder::pagebuilder.Delete')}}</a>';
                        },"className":"action-col","orderable": false,
                        "targets": 6
                    },
                ]
            });
        });

        $(document).on('click','.deleteTranslation',function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            var lang = $(this).data('lang');
            @if(\Illuminate\Support\Facades\Auth::user()->isSuperAdmin())
            doDelete(id,lang);
            @endif
        })


        function doDelete(id,lang) {
            var deleteRoute = "{{route('admin.pagebuilder.destroy',['pagebuilder'=>'sampleId'])}}";
            // if(confirm('You really want to delete this record?')) {
            var lang = lang ? lang: '';
            swal({
                title: "{{__('general.Warning!')}}",
                text: "{{__('general.Are you sure you need to delete this item? this change cannot be undone.')}}",
                icon: "warning",
                buttons: {
                    cancel: {
                        text: "{{__('general.Cancel')}}",
                        value: null,
                        visible: !0,
                        className: "",
                        closeModal: !1
                    },
                    confirm: {
                        text: "{{__('general.Yes.Delete')}}",
                        value: !0,
                        visible: !0,
                        className: "",
                        closeModal: !1
                    }
                }
            }).then(e => {
                if (e) {
                    $.ajax({
                        dataType: 'json',
                        method: 'delete',
                        url: deleteRoute.replace('sampleId',id)+'?lang='+lang,
                    }).done(function (response) {
                        swal("{{__('general.Success!')}}", "{{__('general.Item deleted!!')}}", "success").then(() => {
                            theGrid.ajax.reload();
                        });
                    }).fail(function (erroErrorr) {
                        swal("{{__('general.Error')}}", "{{__('general.Error Occured')}}", "error");
                    });
                } else {
                    swal("{{__('general.Cancelled')}}", "{{__("general.It's safe")}}", "error");
                }
            });
            {{--$.ajax({ url: '{{ url('/instructors') }}/' + id, type: 'DELETE'}).success(function() {--}}
            {{--theGrid.ajax.reload();--}}
            {{--});--}}
            // }
            return false;
        }
    </script>
    <script src="{{ asset("vendors/js/extensions/sweetalert.min.js") }}" type="text/javascript"></script>

@endsection